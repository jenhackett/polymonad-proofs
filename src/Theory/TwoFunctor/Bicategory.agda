 
module Theory.TwoFunctor.Bicategory where

-- Stdlib
open import Level renaming ( suc to lsuc ; zero to lzero )
open import Function hiding ( id ) renaming ( _∘_ to _∘F_ )
open import Data.Product
open import Data.Sum
open import Data.Unit
open import Data.Empty
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.HeterogeneousEquality renaming ( refl to hrefl ; sym to hsym ; trans to htrans ; cong to hcong ; cong₂ to hcong₂ ; subst to hsubst ; subst₂ to hsubst₂ ; proof-irrelevance to het-proof-irrelevance )
open ≡-Reasoning hiding ( _≅⟨_⟩_ )
open ≅-Reasoning renaming ( begin_ to hbegin_ ; _∎ to _∎h ) hiding ( _≡⟨_⟩_ )

-- Local
open import Utilities
open import Extensionality
open import Congruence
open import Theory.Category.Definition
open import Theory.Functor.Definition
open import Theory.Functor.Examples
open import Theory.Functor.Application
open import Theory.Natural.Transformation
open import Theory.TwoCategory.Bicategory
open import Theory.TwoCategory.Examples.CodiscreteHomCat

-------------------------------------------------------------------------------
-- Definition of 2-Functors
-------------------------------------------------------------------------------

open Category hiding ( left-id ; right-id ; assoc ;_∘_ ) renaming ( id to idC )
open Bicategory

record LaxTwoFunctor {ℓC₀ ℓC₁ ℓC₂ ℓD₀ ℓD₁ ℓD₂ : Level} 
                     (C : Bicategory {ℓC₀} {ℓC₁} {ℓC₂}) 
                     (D : Bicategory {ℓD₀} {ℓD₁} {ℓD₂}) 
                     : Set (lsuc (ℓC₀ ⊔ ℓC₁ ⊔ ℓC₂ ⊔ ℓD₀ ⊔ ℓD₁ ⊔ ℓD₂)) where
  constructor lax-two-functor

  private
    _∘D_ = _∘_ D
    _∘C_ = _∘_ C
 
    _∘Cₕ_ = _∘ₕ_ C
    _∘Dₕ_ = _∘ₕ_ D

    _▷D_ = _▷_ D
    _▷C_ = _▷_ C

    _◁D_ = _◁_ D
    _◁C_ = _◁_ C

    _∘Dᵥ_ = _∘ᵥ_ D
    _∘Cᵥ_ = _∘ᵥ_ C

    id₁D = id₁ D

  field
    -- Names and structure base on: https://ncatlab.org/nlab/show/pseudofunctor
    -- Of course, adapted to lax 2-functors.
    
    -- P_{x}
    P₀ : Cell₀ C → Cell₀ D
    -- P_{x,y}
    P₁ : {x y : Cell₀ C} → Functor (HomCat C x y) (HomCat D (P₀ x) (P₀ y))
    
    -- P_{id_x}
    η : {x : Cell₀ C}
      → Cell₂ D (id₁ D (P₀ x)) ([ P₁ {x} {x} ]₀ (id₁ C x))

    -- P_{x,y,z}
    μ : {x y z : Cell₀ C} {f : Cell₁ C x y} {g : Cell₁ C y z}
         -- (F₁ g ∘ F₁ f) ∼ F₁ (g ∘ f)
         → Cell₂ D ([ P₁ ]₀ g  ∘D  [ P₁ ]₀ f) ([ P₁ ]₀ (g ∘C f))
    
    laxFunId₁ : {x y : Cell₀ C} {f : Cell₁ C x y} 
              → [ P₁ ]₁ (r C _) ∘Dᵥ (μ {x} {x} {y} {id₁ C x} {f} ∘Dᵥ (id₂ D ([ P₁ {x} {y} ]₀ f) ∘Dₕ η {x}))
              ≡ r D _
    
    laxFunId₂ : {x y : Cell₀ C} {f : Cell₁ C x y} 
              → [ P₁ ]₁ (l C _) ∘Dᵥ (μ {x} {y} {y} {f} {id₁ C y} ∘Dᵥ (η {y} ∘Dₕ id₂ D ([ P₁ {x} {y} ]₀ f)))
              ≡ l D _

    -- μ ∘ᵥ (id₂ ∘ₕ μ) ≅ μ ∘ᵥ (μ ∘ₕ id₂)
    laxFunAssoc : {w x y z : Cell₀ C} {f : Cell₁ C w x} {g : Cell₁ C x y} {h : Cell₁ C y z}
                → μ {w} {y} {z} {g ∘C f} {h} ∘Dᵥ ((id₂ D {P₀ y} {P₀ z} ([ P₁ {y} {z} ]₀ h) ∘Dₕ μ {w} {x} {y} {f} {g}) ∘Dᵥ α D _ _ _)
                ≡ [ P₁ ]₁ (α C _ _ _) ∘Dᵥ (μ {w} {x} {z} {f} {h ∘C g} ∘Dᵥ (μ {x} {y} {z} {g} {h} ∘Dₕ id₂ D {P₀ w} {P₀ x} ([ P₁ {w} {x} ]₀ f)))
    
    μ-natural₁ : {a b c : Cell₀ C} → (f : Cell₁ C a b) → {x y : Cell₁ C b c} {α : Cell₂ C x y} 
               → [ P₁ ]₁ (α ∘Cₕ id₂ C f) ∘Dᵥ μ {f = f} {x} ≡ μ {f = f} {y} ∘Dᵥ ([ P₁ ]₁ α ∘Dₕ [ P₁ ]₁ (id₂ C f))
    
    μ-natural₂ : {a b c : Cell₀ C} → (g : Cell₁ C b c) {x y : Cell₁ C a b} {α : Cell₂ C x y}
               → [ P₁ ]₁ (id₂ C g ∘Cₕ α) ∘Dᵥ μ {f = x} {g} ≡ μ {f = y} {g} ∘Dᵥ ([ P₁ ]₁ (id₂ C g) ∘Dₕ [ P₁ ]₁ α)

module Equivalence where

  open import Theory.TwoFunctor.Definition renaming (LaxTwoFunctor to LaxTwoFunctor')

  open import Theory.TwoCategory.Definition
  open import Theory.TwoCategory.Examples.ToBicategory
  open StrictTwoCategory

  open import Congruence
  open import ProofIrrelevance
  open import Extensionality
  open import Bijection renaming ( refl to brefl ; sym to bsym ; trans to btrans )

  equivalent-defs : {ℓC₀ ℓC₁ ℓC₂ ℓD₀ ℓD₁ ℓD₂ : Level} 
                     (C : StrictTwoCategory {ℓC₀} {ℓC₁} {ℓC₂}) 
                     (D : StrictTwoCategory {ℓD₀} {ℓD₁} {ℓD₂})
                     → LaxTwoFunctor' C D ↔ LaxTwoFunctor (StrictTwoCategory→Bicategory C) (StrictTwoCategory→Bicategory D)
  equivalent-defs C D = bijection to from to-from from-to
    where
          _∘D_ = StrictTwoCategory._∘_ D
          _∘C_ = StrictTwoCategory._∘_ C
 
          _∘Cₕ_ = StrictTwoCategory._∘ₕ_ C
          _∘Dₕ_ = StrictTwoCategory._∘ₕ_ D

          _▷D_ = StrictTwoCategory._▷_ D
          _▷C_ = StrictTwoCategory._▷_ C

          _◁D_ = StrictTwoCategory._◁_ D
          _◁C_ = StrictTwoCategory._◁_ C

          _∘Dᵥ_ = StrictTwoCategory._∘ᵥ_ D
          _∘Cᵥ_ = StrictTwoCategory._∘ᵥ_ C

          idD = StrictTwoCategory.id₂ D
          idC' = StrictTwoCategory.id₂ C
          to : LaxTwoFunctor' C D → LaxTwoFunctor (StrictTwoCategory→Bicategory C) (StrictTwoCategory→Bicategory D)
          to F = lax-two-functor (LaxTwoFunctor'.P₀ F)
                                 (LaxTwoFunctor'.P₁ F)
                                 (LaxTwoFunctor'.η F)
                                 (LaxTwoFunctor'.μ F)
                                 (LaxTwoFunctor'.laxFunId-λ' F)
                                 (LaxTwoFunctor'.laxFunId-ρ F)
                                 (sym (LaxTwoFunctor'.laxFunAssoc-α' F))
                                 (LaxTwoFunctor'.μ-natural₁ F)
                                 (LaxTwoFunctor'.μ-natural₂ F)
          
          from : LaxTwoFunctor (StrictTwoCategory→Bicategory C) (StrictTwoCategory→Bicategory D) → LaxTwoFunctor' C D
          from F = lax-two-functor (LaxTwoFunctor.P₀ F)
                                   (LaxTwoFunctor.P₁ F)
                                   (LaxTwoFunctor.η F)
                                   (LaxTwoFunctor.μ F)
                                   (hbegin 
                                       (LaxTwoFunctor.μ F) ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F)
                                     ≅⟨ ≡-to-≅ (sym (v-right-id D)) ⟩
                                       (idD ∘Dᵥ ((LaxTwoFunctor.μ F) ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F)))
                                     ≅⟨ hcong (λ f → f ∘Dᵥ ( LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F))) (≡-to-≅ (sym (Functor.id (LaxTwoFunctor.P₁ F)))) ⟩
                                        (([ LaxTwoFunctor.P₁ F ]₁ idC') ∘Dᵥ ( LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F)))
                                     ≅⟨ hcong₂ (λ X Y → (([ LaxTwoFunctor.P₁ F ]₁ (StrictTwoCategory.Cell₂ C (_ ∘C id₁ C) X ∋ Y)) ∘Dᵥ ( LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F)))) (≡-to-≅ (left-id C)) (htrans (id≅id C (left-id C)) (hsym (λ'≅id₂ C _))) ⟩
                                        (([ LaxTwoFunctor.P₁ F ]₁ (λ' C _) ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.η F))))
                                     ≅⟨  ≡-to-≅ (LaxTwoFunctor.laxFunId₁ F) ⟩
                                        λ' D _
                                     ≅⟨ λ'≅id₂ D _ ⟩
                                        idD ∎h)
                                   (hbegin
                                       (LaxTwoFunctor.μ F) ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD)
                                     ≅⟨ ≡-to-≅ (sym (v-right-id D)) ⟩
                                       (idD ∘Dᵥ ( (LaxTwoFunctor.μ F) ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD)))
                                     ≅⟨ hcong
                                          (λ f → f ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD))) (≡-to-≅ $ sym (Functor.id (LaxTwoFunctor.P₁ F))) ⟩
                                       ([ LaxTwoFunctor.P₁ F ]₁ idC' ∘Dᵥ ( (LaxTwoFunctor.μ F) ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD)))
                                     ≅⟨ hcong₂
                                          (λ X Y →
                                             [ LaxTwoFunctor.P₁ F ]₁
                                             (StrictTwoCategory.Cell₂ C (id₁ C ∘C _) X ∋ Y)
                                             ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD)))
                                          (≡-to-≅ (right-id C)) (htrans (id≅id C (right-id C)) (hsym (ρ≅id₂ C _))) ⟩
                                       (([ LaxTwoFunctor.P₁ F ]₁ (ρ C _) ∘Dᵥ ( (LaxTwoFunctor.μ F) ∘Dᵥ (LaxTwoFunctor.η F ∘Dₕ idD))))
                                     ≅⟨ ≡-to-≅ (LaxTwoFunctor.laxFunId₂ F) ⟩
                                       ρ D _
                                     ≅⟨ ρ≅id₂ D _ ⟩
                                       idD ∎h)
                                   (hbegin (
                                       (LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.μ F))
                                     ≅⟨  ≡-to-≅ (sym (v-left-id D)) ⟩
                                       (LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.μ F)) ∘Dᵥ idD
                                     ≅⟨ hcong₂
                                          (λ X Y → (LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.μ F)) ∘Dᵥ (StrictTwoCategory.Cell₂ D X ([ LaxTwoFunctor.P₁ F ]₀ _ ∘D ([ LaxTwoFunctor.P₁ F ]₀ _ ∘D [ LaxTwoFunctor.P₁ F ]₀ _)) ∋ Y)) (≡-to-≅ (assoc D)) (htrans (id≅id D (assoc D)) (hsym (α'≅id₂ D _ _ _))) ⟩
                                        (LaxTwoFunctor.μ F ∘Dᵥ (idD ∘Dₕ LaxTwoFunctor.μ F)) ∘Dᵥ StrictTwoCategory.α' D _ _ _
                                     ≅⟨ ≡-to-≅ (sym (v-assoc D)) ⟩
                                       LaxTwoFunctor.μ F ∘Dᵥ ((idD ∘Dₕ LaxTwoFunctor.μ F) ∘Dᵥ StrictTwoCategory.α' D _ _ _)
                                     ≅⟨ ≡-to-≅ (LaxTwoFunctor.laxFunAssoc F) ⟩
                                       ([ LaxTwoFunctor.P₁ F ]₁ (StrictTwoCategory.α' C _ _ _) ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.μ F ∘Dₕ idD)))
                                     ≅⟨ hcong₂ (λ X Y → ([ LaxTwoFunctor.P₁ F ]₁ ((StrictTwoCategory.Cell₂ C ((_ ∘C _) ∘C _) X) ∋ Y) ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.μ F ∘Dₕ idD))))
                                        (≡-to-≅ (assoc C)) (α'≅id₂ C _ _ _) ⟩
                                       ([ LaxTwoFunctor.P₁ F ]₁ idC' ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.μ F ∘Dₕ idD)))
                                     ≅⟨ ≡-to-≅ (trans (cong
                                                         (λ f → f ∘Dᵥ (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.μ F ∘Dₕ idD)))
                                                         (Functor.id (LaxTwoFunctor.P₁ F))) (v-right-id D)) ⟩
                                       (LaxTwoFunctor.μ F ∘Dᵥ (LaxTwoFunctor.μ F ∘Dₕ idD)) ∎h)) 
                                   (LaxTwoFunctor.μ-natural₁ F)
                                   (LaxTwoFunctor.μ-natural₂ F)
          
          to-from : (F : _) → to (from F) ≡ F
          to-from (lax-two-functor P₀ P₁ η μ laxFunId₁ laxFunId₂ laxFunAssoc μ-natural₁ μ-natural₂) = cong₅ (lax-two-functor P₀ P₁ η μ) (implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≡ _ _) ((implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≡ _ _)) (implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x₃ → implicit-fun-ext λ x₄ → implicit-fun-ext λ x₅ → implicit-fun-ext λ x₆ → proof-irr-≡ _ _) refl refl

          from-to : (F : _) → from (to F) ≡ F
          from-to (lax-two-functor P₀ P₁ η μ laxFunId₁ laxFunId₂ laxFunAssoc μ-natural₁ μ-natural₂) = cong₅ (lax-two-functor P₀ P₁ η μ) (implicit-fun-ext  λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≅ _ _) ((implicit-fun-ext  λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≅ _ _)) (implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x₃ → implicit-fun-ext λ x₄ → implicit-fun-ext λ x₅ → implicit-fun-ext λ x₆ → proof-irr-≅ _ _) refl refl
