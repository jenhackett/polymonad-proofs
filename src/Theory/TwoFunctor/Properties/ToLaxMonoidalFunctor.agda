
-- Stdlib
open import Level
open import Function renaming ( _∘_ to _∘F_ ; id to idF )
open import Data.Unit
open import Data.Product
open import Data.Unit
open import Data.Empty
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.HeterogeneousEquality renaming ( refl to hrefl ; sym to hsym ; trans to htrans ; cong to hcong ; cong₂ to hcong₂ ; subst₂ to hsubst₂ ; proof-irrelevance to hproof-irrelevance )
open ≡-Reasoning hiding ( _≅⟨_⟩_ )
open ≅-Reasoning hiding ( _≡⟨⟩_ ; _≡⟨_⟩_ ) renaming ( begin_ to hbegin_ ; _∎ to _∎h )

-- Local
open import Extensionality
open import Utilities
open import Theory.Monoid
open import Theory.Category.Definition
open import Theory.Category.Examples.Monoid
open import Theory.Category.Examples.Discrete
open import Theory.Category.Examples.Functor
open import Theory.Category.Monoidal
open import Theory.Category.Monoidal.Examples.Monoid
open import Theory.Category.Monoidal.Examples.FunctorWithComposition renaming ( functorMonoidalCategory to Fun )
open import Theory.Functor.Definition hiding ( functor )
open import Theory.Functor.Composition
open import Theory.Functor.Monoidal
open import Theory.Natural.Transformation
open import Theory.Natural.Transformation.Examples
open import Theory.Natural.Transformation.Examples.FunctorCompositionAssociator
open import Theory.TwoCategory.Bicategory
open import Theory.TwoCategory.Bicategory.Examples.FromMonoidalCategory
open import Theory.TwoFunctor.ConstZeroCell
open import Theory.TwoFunctor.Bicategory

open Category hiding ( left-id ; right-id ; assoc )
open Bicategory
open LaxTwoFunctor

module Theory.TwoFunctor.Properties.ToLaxMonoidalFunctor where 

open MonoidalCategory

LaxTwoFunctor→LaxMonoidalFunctor : {ℓC₀ ℓC₁ : Level}
                                 → {C D : Category {ℓC₀} {ℓC₁}}
                                 → (MC : MonoidalCategory C)
                                 → (MD : MonoidalCategory D)
                                 → LaxTwoFunctor (MonoidalCategory→Bicategory MC)
                                                 (MonoidalCategory→Bicategory MD)
                                 → LaxMonoidalFunctor MC MD
LaxTwoFunctor→LaxMonoidalFunctor {ℓC₀} {ℓC₁} {C} {D} MC MD F
  = laxMonoidalFunctor F' ε μ-nat assoc' left-unit right-unit
  where
    _∘D_ = Category._∘_ D
    _⊗D_ = _⊗₁_ MD
    _⊗ₒD_ = _⊗₀_ MD
    _∘C_ = Category._∘_ C
    _⊗C_ = _⊗₁_ MC
    _⊗₀C_ = _⊗₀_ MC

    F' : Functor C D
    F' = P₁ F

    ε : Hom D (unit MD) ([ F' ]₀ (unit MC))
    ε = η F

    μ₀ : (x : Obj (C ×C C)) → Hom D ([ [ tensor MD ]∘[ [ F' ]×[ F' ] ] ]₀ x) ([ [ F' ]∘[ tensor MC ] ]₀ x)
    μ₀ (l , r) = μ F {f = r} {g = l}
     
    μ-natural : {a b : Obj (C ×C C)} {f : Hom (C ×C C) a b} →
               ([ [ F' ]∘[ tensor MC ] ]₁ f) ∘D (μ₀ a) ≡
               (μ₀ b) ∘D ([ [ tensor MD ]∘[ [ F' ]×[ F' ] ] ]₁ f)
    μ-natural {a₁ , a₂} {b₁ , b₂} {f₁ , f₂} =
       begin [ F' ]₁ (f₁ ⊗C f₂) ∘D (μ₀ (a₁ , a₂))
         ≡⟨ cong (λ f → [ F' ]₁ f ∘D μ₀ (a₁ , a₂)) (cong₂ _⊗C_ (sym (left-id MC)) (sym (right-id MC))) ⟩
             [ F' ]₁ ((f₁ ∘C id C) ⊗C (id C ∘C f₂)) ∘D (μ₀ (a₁ , a₂))
         ≡⟨ cong (λ f → ([ F' ]₁ f) ∘D μ₀ (a₁ , a₂)) (exchange MC) ⟩
             [ F' ]₁ ((f₁ ⊗C id C) ∘C (id C ⊗C f₂)) ∘D (μ₀ (a₁ , a₂))
         ≡⟨ cong (λ f → f ∘D (μ₀ (a₁ , a₂))) (Functor.compose F') ⟩
             ([ F' ]₁ (f₁ ⊗C id C) ∘D [ F' ]₁ (id C ⊗C f₂)) ∘D (μ₀ (a₁ , a₂))
         ≡⟨ sym (assoc MD) ⟩
           ([ F' ]₁ (f₁ ⊗C id C) ∘D ([ F' ]₁ (id C ⊗C f₂) ∘D (μ₀ (a₁ , a₂))))
         ≡⟨ cong (λ f → [ F' ]₁ (f₁ ⊗C id C) ∘D f) (μ-natural₂ F a₁) ⟩
           ([ F' ]₁ (f₁ ⊗C id C) ∘D (μ₀ (a₁ , b₂) ∘D ([ F' ]₁ (id C) ⊗D [ F' ]₁ f₂)))
         ≡⟨ assoc MD ⟩
           (([ F' ]₁ (f₁ ⊗C id C) ∘D μ₀ (a₁ , b₂)) ∘D ([ F' ]₁ (id C) ⊗D [ F' ]₁ f₂))
         ≡⟨ cong (λ f → f ∘D ([ F' ]₁ (id C) ⊗D [ F' ]₁ f₂)) (μ-natural₁ F b₂) ⟩
           (((μ₀ ((b₁ , b₂))) ∘D (([ F' ]₁ f₁) ⊗D [ F' ]₁ (id C))) ∘D ([ F' ]₁ (id C) ⊗D [ F' ]₁ f₂))
         ≡⟨ sym (assoc MD) ⟩
           (((μ₀ ((b₁ , b₂))) ∘D ((([ F' ]₁ f₁) ⊗D [ F' ]₁ (id C)) ∘D ([ F' ]₁ (id C) ⊗D [ F' ]₁ f₂))))
         ≡⟨ cong (λ f → μ₀ (b₁ , b₂) ∘D f) (sym (exchange MD)) ⟩
           (μ₀ ((b₁ , b₂))) ∘D ((([ F' ]₁ f₁) ∘D [ F' ]₁ (id C)) ⊗D ([ F' ]₁ (id C) ∘D [ F' ]₁ f₂))
         ≡⟨ cong (λ f → μ₀ (b₁ , b₂) ∘D f) (cong₂ _⊗D_ (trans (sym (Functor.compose F')) (cong [ F' ]₁ (left-id MC))) (trans (sym (Functor.compose F')) (cong [ F' ]₁ (right-id MC)))) ⟩
           (μ₀ (b₁ , b₂) ∘D ([ [ tensor MD ]∘[ [ F' ]×[ F' ] ] ]₁ (f₁ , f₂))) ∎

    μ-nat : NaturalTransformation [ tensor MD ]∘[ [ F' ]×[ F' ] ] [ F' ]∘[ tensor MC ]
    μ-nat = naturalTransformation μ₀ μ-natural

    assoc' = λ x y z → sym (laxFunAssoc F)

    left-unit = λ x → sym (laxFunId₂ F)

    right-unit = λ x → sym (laxFunId₁ F)
