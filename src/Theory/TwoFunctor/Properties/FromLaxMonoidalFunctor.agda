
-- Stdlib
open import Level
open import Function renaming ( _∘_ to _∘F_ ; id to idF )
open import Data.Unit
open import Data.Product
open import Data.Unit
open import Data.Empty
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.HeterogeneousEquality renaming ( refl to hrefl ; sym to hsym ; trans to htrans ; cong to hcong ; cong₂ to hcong₂ ; subst₂ to hsubst₂ ; proof-irrelevance to hproof-irrelevance )
open ≡-Reasoning hiding ( _≅⟨_⟩_ )
open ≅-Reasoning hiding ( _≡⟨⟩_ ; _≡⟨_⟩_ ) renaming ( begin_ to hbegin_ ; _∎ to _∎h )

-- Local
open import Extensionality
open import Utilities
open import Theory.Monoid
open import Theory.Category.Definition
open import Theory.Category.Examples.Monoid
open import Theory.Category.Examples.Discrete
open import Theory.Category.Examples.Functor
open import Theory.Category.Monoidal
open import Theory.Category.Monoidal.Examples.Monoid
open import Theory.Category.Monoidal.Examples.FunctorWithComposition renaming ( functorMonoidalCategory to Fun )
open import Theory.Functor.Definition hiding ( functor )
open import Theory.Functor.Composition
open import Theory.Functor.Monoidal
open import Theory.Natural.Transformation
open import Theory.Natural.Transformation.Examples
open import Theory.Natural.Transformation.Examples.FunctorCompositionAssociator
open import Theory.TwoCategory.Bicategory
open import Theory.TwoCategory.Bicategory.Examples.FromMonoidalCategory
open import Theory.TwoFunctor.Bicategory

open Category hiding ( left-id ; right-id ; assoc )

module Theory.TwoFunctor.Properties.FromLaxMonoidalFunctor where 

open Bicategory
open MonoidalCategory

LaxMonoidalFunctor→LaxTwoFunctor : {ℓC₀ ℓC₁ : Level}
                                 → {C D : Category {ℓC₀} {ℓC₁}}
                                 → (MC : MonoidalCategory C)
                                 → (MD : MonoidalCategory D)
                                 → LaxMonoidalFunctor MC MD
                                 → LaxTwoFunctor (MonoidalCategory→Bicategory MC) (MonoidalCategory→Bicategory MD)
LaxMonoidalFunctor→LaxTwoFunctor {ℓC₀} {ℓC₁} {Eff} C D MonF
  = lax-two-functor (λ tt → tt) (LaxMonoidalFunctor.F MonF) (LaxMonoidalFunctor.ε MonF) (λ {x} {y} {z} {f} {g} → LaxMonoidalFunctor.μ MonF g f) (sym (LaxMonoidalFunctor.right-unitality MonF _)) (sym (LaxMonoidalFunctor.left-unitality MonF _)) (sym (LaxMonoidalFunctor.assoc MonF _ _ _)) (λ f → NaturalTransformation.natural (LaxMonoidalFunctor.μ-natural-transformation MonF)) ((λ f → NaturalTransformation.natural (LaxMonoidalFunctor.μ-natural-transformation MonF)))
