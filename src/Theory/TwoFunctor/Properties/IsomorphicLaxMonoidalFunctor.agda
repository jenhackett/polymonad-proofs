

-- Stdlib
open import Level
open import Function renaming ( _∘_ to _∘F_ ; id to idF )
open import Data.Unit
open import Data.Product
open import Data.Unit
open import Data.Empty
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.HeterogeneousEquality renaming ( refl to hrefl ; sym to hsym ; trans to htrans ; cong to hcong ; cong₂ to hcong₂ ; subst₂ to hsubst₂ ; proof-irrelevance to hproof-irrelevance )
open ≡-Reasoning hiding ( _≅⟨_⟩_ )
open ≅-Reasoning hiding ( _≡⟨⟩_ ; _≡⟨_⟩_ ) renaming ( begin_ to hbegin_ ; _∎ to _∎h )

-- Local
open import Extensionality
open import Bijection renaming ( refl to brefl )
open import Utilities
open import Theory.Monoid
open import Theory.Category.Definition
open import Theory.Category.Examples.Monoid
open import Theory.Category.Examples.Discrete
open import Theory.Category.Examples.Functor
open import Theory.Category.Monoidal
open import Theory.Category.Monoidal.Examples.Monoid
open import Theory.Category.Monoidal.Examples.FunctorWithComposition renaming ( functorMonoidalCategory to Fun )
open import Theory.Functor.Definition hiding ( functor )
open import Theory.Functor.Composition
open import Theory.Functor.Monoidal
open import Theory.Natural.Transformation
open import Theory.Natural.Transformation.Examples
open import Theory.Natural.Transformation.Examples.FunctorCompositionAssociator
open import Theory.TwoCategory.Bicategory
open import Theory.TwoCategory.Bicategory.Examples.FromMonoidalCategory
open import Theory.TwoFunctor.Bicategory
open import Theory.TwoFunctor.Properties.FromLaxMonoidalFunctor
open import Theory.TwoFunctor.Properties.ToLaxMonoidalFunctor

open Category hiding ( left-id ; right-id ; assoc )
open Bicategory
open import Congruence
open import ProofIrrelevance
open import Extensionality
 
module Theory.TwoFunctor.Properties.IsomorphicLaxMonoidalFunctor where

LaxMonoidalFunctor↔LaxTwoFunctor : {ℓC₀ ℓC₁ : Level}
                                 → {C D : Category {ℓC₀} {ℓC₁}}
                                 → (MC : MonoidalCategory C)
                                 → (MD : MonoidalCategory D)
                                 → LaxTwoFunctor (MonoidalCategory→Bicategory MC)
                                                 (MonoidalCategory→Bicategory MD)
                                 ↔ LaxMonoidalFunctor MC MD
                                  
LaxMonoidalFunctor↔LaxTwoFunctor {ℓC₀} {ℓC₁} MC MD
  = bijection (LaxTwoFunctor→LaxMonoidalFunctor MC MD) (LaxMonoidalFunctor→LaxTwoFunctor MC MD) id' id''
  where
    id' : (MonF : LaxMonoidalFunctor MC MD)
         → LaxTwoFunctor→LaxMonoidalFunctor MC MD
         (LaxMonoidalFunctor→LaxTwoFunctor MC MD MonF) ≡ MonF
    id' b = lax-monoidal-functor-eq refl hrefl (het-natural-transformation-eq refl refl hrefl)
    id'' : (F : LaxTwoFunctor (MonoidalCategory→Bicategory MC)
                                (MonoidalCategory→Bicategory MD))
         → LaxMonoidalFunctor→LaxTwoFunctor MC MD
         (LaxTwoFunctor→LaxMonoidalFunctor MC MD F) ≡ F
    id'' (lax-two-functor P₀ P₁ η μ laxFunId₁ laxFunId₂ laxFunAssoc μ-natural₁ μ-natural₂) = cong₅ (lax-two-functor P₀ P₁ η μ) (implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≡ _ _) ((implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → proof-irr-≡ _ _)) ((implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x → proof-irr-≡ _ _)) ((implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → fun-ext λ x₂ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x → implicit-fun-ext λ x → proof-irr-≡ _ _)) ((implicit-fun-ext λ x → implicit-fun-ext λ x₁ → implicit-fun-ext λ x₂ → fun-ext λ x₂ → implicit-fun-ext λ x₂ → implicit-fun-ext λ x → implicit-fun-ext λ x → proof-irr-≡ _ _))
